from argparse import ArgumentParser
import copy
import pathlib
import pickle as pickle
from typing import Tuple
from tqdm import tqdm

import music21 as m21
from music21.chord import ChordException
import numpy as np


def _get_note_on_string(chord: m21.chord.Chord, string: int):
    for note in chord.notes:
        if note.string.number == string:
            return note
    # a chord can be tied to another one that do not share all notes
    return None

def slide_remover_stream(stream: m21.stream.Stream) -> m21.stream.Stream:
    out = copy.deepcopy(stream)
    for note in out.flatten().notes:
        for art in note.articulations:
            if isinstance(art, m21.articulations.IndeterminateSlide):
                note.articulations = [a for a in note.articulations if a != art]
        if isinstance(note, m21.chord.Chord):
            for n in note.notes:
                for art in n.articulations:
                    if isinstance(art, m21.articulations.IndeterminateSlide):
                        n.articulations = [a for a in n.articulations if a != art]
    return out


def slide_remover(note: m21.note.Note):
    """
    Removes any slide articulation if present.

    args:
        note object to process
    """
    if isinstance(note, m21.note.Note):
        for art in note.articulations:
            if art.name == 'indeterminate slide':
                note.articulations = [a for a in note.articulations if a != art]


def bend_remover_stream(stream: m21.stream.Stream, replace_with_destination: bool = True) -> m21.stream.Stream:
    """bend_remover_stream.

    Args:
        stream (m21.stream.Stream): stream
        replace_with_destination (bool): replace_with_destination

    Returns:
        m21.stream.Stream:
    """
    out = copy.deepcopy(stream)
    for note in out.flatten().notes:
        bend_remover(note, replace_with_destination=replace_with_destination)
    return out



def bend_remover(note: m21.note.Note, replace_with_destination: bool = True):
    """
    Remove bend by replacing note value either by the origin or the destination of the bend.

    args:
        note to process;
        replace_with_destination: if True, the new value is the destination of the bend. Default is True.
    """
    if isinstance(note, m21.note.Note):
        if note.bend is not None:
            if replace_with_destination:
                bend_size = note.bend.bendAlter
                note.transpose(bend_size, inPlace=True)
                # refresh fret indication
                note.fret.number += bend_size.semitones
            note.articulations = [a for a in note.articulations if a != note.bend]
    else:
        for n in note.notes:
            if n.bend is not None:
                bend_remover(n, replace_with_destination=replace_with_destination)
        return note
            

def _bend_remover(not_rest: m21.note.NotRest) -> Tuple[m21.note.NotRest | None, bool]:
    out = m21.common.misc.defaultDeepcopy(not_rest)
    tied_to_next = False
    if out.tie is not None and out.tie.type in ["start", "continue"]:
        tied_to_next = True
    if out.isChord:
        bend_detected = False
        for note in out.notes:
            if note.bend is not None:
                bend_detected = True
                out = _remove_bend_inside_chord(note, out)
        if not bend_detected:
            return None, tied_to_next
    else:
        if out.bend is not None:
            out = _remove_bend_single_note(out)
        else:
            return None, tied_to_next
    return out, tied_to_next


def _remove_bend_inside_chord(note: m21.note.Note, chord: m21.chord.Chord | list) -> m21.chord.Chord:
    memory = m21.common.misc.defaultDeepcopy(note)
    if isinstance(chord, list):
        # second bend in chord with U&D bend
        for c in chord:
            c = _remove_bend_inside_chord(note, c)
            if isinstance(c, list):
                # two U&D bends in chord
                first_chord = c[0]
                first_chord.duration.augmentOrDiminish(2)
                complete_notes = [a for a in chord[1].notes if a != memory]
                for tmp_note in c[1].notes:
                    tmp_note.duration.augmentOrDiminish(2)
                    occupied_string = False
                    if tmp_note not in complete_notes:
                        for b in complete_notes:
                            if b.string == tmp_note.string:
                                occupied_string = True
                                break
                        if not occupied_string:
                            complete_notes.append(tmp_note)
                second_chord = m21.chord.Chord(notes=complete_notes)
                chord = [first_chord, second_chord]
                break
    else:
        for n in chord.notes:
            if n == memory:
                tmp = m21.common.misc.defaultDeepcopy(n)
                out = _remove_bend_single_note(tmp)
                if isinstance(out, list):
                    notes_0 = [a for a in chord.notes if a != memory]
                    notes_0.append(out[0])
                    notes_1 = [a for a in chord.notes if a != memory]
                    notes_1.append(out[1])
                    new_chord_0 = m21.chord.Chord(notes=notes_0)
                    new_chord_1 = m21.chord.Chord(notes=notes_1)
                    new_chord_0.duration = out[0].duration
                    new_chord_1.duration = out[1].duration
                    new_chords = [new_chord_0, new_chord_1]
                    chord = new_chords
                else:
                    notes = [a for a in chord.notes if a != memory]
                    notes.append(out)
                    chord = m21.chord.Chord(notes=notes)
                    chord.duration = out.duration
    return chord


def _remove_bend_single_note(note: m21.note.Note):
    """_remove_bend_single_note.
    Helper function to remove the bend articulation of a note and change its pitch and fret accordingly.
    Held bends and Basic Upward Bends are distinguished even if the operation is the same
    because, in the future, a BUB might be replaced by several notes if the bend is slow.

    Args:
        note (m21.note.Note): note
    """
    if not note.bend.preBend:
        if note.bend.release is None:
            # basic upward bend
            note.transpose(note.bend.bendAlter, inPlace=True)
            note.fret.number += int(note.bend.bendAlter.semitones)
            note = _remove_bend_articulation(note)
        else:
            # up-and-down bend
            notes = [m21.common.misc.defaultDeepcopy(note),
                     m21.common.misc.defaultDeepcopy(note)]
            notes[0].duration = notes[0].duration.augmentOrDiminish(0.5)
            notes[1].duration = notes[1].duration.augmentOrDiminish(0.5)
            notes[0].transpose(note.bend.bendAlter, inPlace=True)
            notes[0].fret.number += int(note.bend.bendAlter.semitones)
            notes[0] = _remove_bend_articulation(notes[0])
            notes[1] = _remove_bend_articulation(notes[1])
            note = notes
    else:
        if note.bend.release is None:
            # held bend
            note.transpose(note.bend.bendAlter, inPlace=True)
            note.fret.number += int(note.bend.bendAlter.semitones)
            note = _remove_bend_articulation(note)
        else:
            # reverse bend
            note = _remove_bend_articulation(note)
    return note


def _remove_bend_articulation(note: m21.note.Note) -> m21.note.Note:
    art = note.articulations
    out = []
    for a in art:
        if not isinstance(a, m21.articulations.FretBend):
            out.append(a)
    note.articulations = out
    return note


def _check_tied_notes_consistency(current: m21.note.NotRest, previous: m21.note.NotRest):
    out = m21.common.misc.defaultDeepcopy(current)
    if current.isChord and not previous.isChord:
        note = _get_note_on_string(out, previous.string.number)
    elif not(current.isChord) and previous.isChord:
        previous = _get_note_on_string(previous, out.string.number)
        note = out
    elif not(current.isChord) and not(previous.isChord):
        note = out
    elif current.isChord and previous.isChord:
        for n in out.notes:
            n = _check_tied_notes_consistency(n, previous)
        return out
    else:
        raise ValueError("This should never happen")
    if note is None or previous is None:
        return out
    if note.pitch != previous.pitch:
        note.pitch = previous.pitch
    if note.fret.number != previous.fret.number:
        note.fret.number = previous.fret.number
    return out

        


def bend_remover_v2(stream: m21.stream.Score) -> m21.stream.Score:
    """bend_remover_v2.
    voice is copied, modified then replaced because bend removal can 
    replace a single note by two consecutive notes (e.g. up-and-down bending).

    Args:
        stream (m21.stream.Score): stream

    Returns:
        m21.stream.Score:
    """
    for part in stream.parts:
        # I have to put it here otherwise there cannot be a tie across measures
        tied_to_next = False
        memory = None
        for meas in part.getElementsByClass(m21.stream.Measure):
            for voice in meas.voices:
                for not_rest in voice.notes:
                    replacement, tied_to_next = _bend_remover(not_rest)
                    if replacement is None and memory is not None:
                        # check if there are no problem with ties
                        replacement = _check_tied_notes_consistency(not_rest, memory)
                    if replacement is not None:
                        if isinstance(replacement, list):
                            voice.replace(not_rest, replacement[0])
                            insertion_offset = replacement[0].getOffsetBySite(voice) +\
                                                replacement[0].duration.quarterLength
                            for i in range(1, len(replacement)):
                                voice.insert(insertion_offset, replacement[i])
                                insertion_offset += replacement[i].duration.quarterLength
                        else:
                            voice.replace(not_rest, replacement)
                    if tied_to_next:
                        if isinstance(replacement, list):
                            memory = replacement[-1]
                        else:
                            memory = replacement
                    else:
                        memory = None
    return stream
    

def main(parser: ArgumentParser) -> int:
    args = vars(parser.parse_args())
    SOURCEPATH = pathlib.Path(args['source'])
    DESTPATH = pathlib.Path(args['dest'])
    DESTPATH.mkdir(parents=True, exist_ok=True)
    if SOURCEPATH.is_file():
        file = SOURCEPATH
        if (DESTPATH / file.name).exists() and not args['overwrite']:
            return 0
        s = m21.converter.thaw(fp=file)
        # hopo_remover(s)
        # s = slide_remover_stream(s)
        s = bend_remover_v2(s)
        m21.converter.freeze(s,
                fp=str(DESTPATH / file.name))
    for file in tqdm(SOURCEPATH.glob('*.pickle'), total=2244):
        if (DESTPATH / file.name).exists() and not args['overwrite']:
            continue
        s = m21.converter.thaw(fp=file)
        # hopo_remover(s)
        # s = slide_remover_stream(s)
        s = bend_remover_v2(s)
        m21.converter.freeze(s,
                fp=str(DESTPATH / file.name))
    return 0



if __name__ == '__main__':
    parser = ArgumentParser(description="Script to open pickle files, remove articulations and save the cleaned files.")
    parser.add_argument('--source', '-s', type=str,
                        help="Path to the files to parse.")
    parser.add_argument('--dest', '-d', type=str,
                        help="Path to store the parsed files.")
    parser.add_argument('--overwrite', '-o', action='store_true',
                        help="Overwrite the destination file if it exists.")
    main(parser) 
