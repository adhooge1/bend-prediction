import music21 as m21
import pandas as pd
from argparse import ArgumentParser
import pathlib
from util.techniques_remover import _bend_remover
import pickle
from tqdm import tqdm
from util.bend_analysis import _process_notRest
import statistics

def num_notes(note_or_chord: m21.note.NotRest) -> int:
    if isinstance(note_or_chord, m21.note.Note):
        return 1
    else:
        return len(note_or_chord.notes)

def mean_fret(note_or_chord: m21.note.NotRest) -> float:
    if isinstance(note_or_chord, m21.note.Note):
        return note_or_chord.fret.number
    else:
        total_fret = 0
        num_valid_notes = 0
        for note in note_or_chord.notes:
            if note.fret.number != 0:
                total_fret += note.fret.number
                num_valid_notes += 1
        if num_valid_notes == 0:
            return 0
        else:
            return total_fret / num_valid_notes

def mean_string(note_or_chord: m21.note.NotRest) -> float:
    if isinstance(note_or_chord, m21.note.Note):
        return note_or_chord.string.number
    else:
        mean_string = 0
        for note in note_or_chord.notes:
            mean_string += note.string.number
        return mean_string / num_notes(note_or_chord)

def min_midi_pitch(note_or_chord: m21.note.NotRest) -> float:
    if isinstance(note_or_chord, m21.note.Note):
        if note_or_chord.bend is None or note_or_chord.bend.release is not None:
            return note_or_chord.pitch.midi
        else:
            note_or_chord.transpose(note_or_chord.bend.bendAlter)
            return note_or_chord.pitch.midi
    else:
        valid_notes = []
        for note in note_or_chord.notes:
            if note.bend is None or note.bend.release is not None:
                valid_notes.append(note)
            else:
                valid_notes.append(note.transpose(note.bend.bendAlter))
        if len(valid_notes) == 0:
            return 0
        pitches = [note.pitch.midi for note in valid_notes]
        return min(pitches)

def std_midi_pitch(note_or_chord: m21.note.NotRest) -> float:
    if isinstance(note_or_chord, m21.note.Note):
        if note_or_chord.bend is None or note_or_chord.bend.release is not None:
            return note_or_chord.pitch.midi
        else:
            note_or_chord.transpose(note_or_chord.bend.bendAlter)
            return note_or_chord.pitch.midi
    else:
        valid_notes = []
        for note in note_or_chord.notes:
            if note.bend is None or note.bend.release is not None:
                valid_notes.append(note)
            else:
                valid_notes.append(note.transpose(note.bend.bendAlter))
        if len(valid_notes) == 0:
            return 0
        pitches = [note.pitch.midi for note in valid_notes]
        return statistics.pstdev(pitches)

def mean_midi_pitch(note_or_chord: m21.note.NotRest) -> float:
    if isinstance(note_or_chord, m21.note.Note):
        if note_or_chord.bend is None or note_or_chord.bend.release is not None:
            return note_or_chord.pitch.midi
        else:
            note_or_chord.transpose(note_or_chord.bend.bendAlter)
            return note_or_chord.pitch.midi
    else:
        valid_notes = []
        for note in note_or_chord.notes:
            if note.bend is None or note.bend.release is not None:
                valid_notes.append(note)
            else:
                valid_notes.append(note.transpose(note.bend.bendAlter))
        if len(valid_notes) == 0:
            return 0
        return sum(note.pitch.midi for note in valid_notes) / len(valid_notes)


def duration(note_or_chord: m21.note.NotRest) -> float:
    return float(round(note_or_chord.duration.quarterLength, 4))


def longer_than_previous(current: m21.note.NotRest, previous: m21.note.NotRest) -> bool:
    return duration(current) > duration(previous)

def shorter_than_previous(current: m21.note.NotRest, previous: m21.note.NotRest) -> bool:
    return duration(current) < duration(previous)

def same_duration_as_previous(current: m21.note.NotRest, previous: m21.note.NotRest) -> bool:
    return duration(current) == duration(previous)


def fret_jump(current: m21.note.NotRest, previous: m21.note.NotRest) -> float:
    return mean_fret(current) - mean_fret(previous)

def string_jump(current: m21.note.NotRest, previous: m21.note.NotRest) -> float:
    return mean_string(current) - mean_string(previous)

def pitch_jump(current: m21.note.NotRest, previous: m21.note.NotRest) -> float:
    return mean_midi_pitch(current) - mean_midi_pitch(previous)

def beat_strength(note_or_chord: m21.note.NotRest) -> float:
    out = note_or_chord.beatStrength
    return float(round(out, 4))


def pc_wrt_tonic(note_or_chord: m21.note.NotRest, tonic: int):
    if not note_or_chord.isChord:
        return (note_or_chord.pitch.pitchClass - tonic)%12
    else:
        out = []
        for pitch in note_or_chord.pitches:
            out.append((pitch.pitchClass - tonic)%12)
        return sum(out)/len(out)

def make_feat_dictionary(current: m21.note.NotRest, previous1: m21.note.NotRest | None,
        previous2: m21.note.NotRest | None, next1: m21.note.NotRest | None,
        next2: m21.note.NotRest | None,
        key: m21.key.KeySignature,
        num_string: int = 6, num_fret: int = 24, max_pitch: int = 88, max_duration: int = 4,
        min_pitch = 40):
    dico = {}
    tonic = key.getScale(mode='minor').getTonic().pitchClass
    # Current features
    dico['num_notes'] = num_notes(current) / num_string
    dico['mean_pitch'] = (mean_midi_pitch(current) - min_pitch) / (max_pitch - min_pitch)
    dico['duration'] = duration(current) / max_duration
    dico['beat_strength'] = beat_strength(current)
    dico['no_n-1'] = previous1 is None
    dico['no_n-2'] = previous2 is None
    dico['no_n+1'] = next1 is None
    dico['no_n+2'] = next2 is None
    dico['accidentals'] = key.sharps / 7 # if key.sharps is not None else 0
    dico['pc_wrt_tonic'] = pc_wrt_tonic(current, tonic) / 12
    # Diff features
    if previous1 is not None:
        dico['longer'] = longer_than_previous(current, previous1)
        dico['shorter'] = shorter_than_previous(current, previous1)
        dico['equal'] = same_duration_as_previous(current, previous1)
        dico['pitch_jump-1'] = pitch_jump(current, previous1) / (max_pitch - min_pitch)
        dico['fret-1'] = mean_fret(previous1) / num_fret
        dico['string-1'] = (mean_string(previous1)) / (num_string - 1)
        dico['pitch-1'] = (mean_midi_pitch(previous1)-min_pitch) / (max_pitch - min_pitch)
    else:
        dico['longer'] = False
        dico['shorter'] = False
        dico['equal'] = False
        dico['pitch_jump-1']= 0
        dico['fret-1'] = 0
        dico['string-1'] = 0
        dico['pitch-1'] = 0
    if previous2 is not None:
        dico['fret_jump-2'] = fret_jump(previous1, previous2) / num_fret
        dico['string_jump-2'] = string_jump(previous1, previous2) / (num_string - 1)
        dico['pitch_jump-2'] = pitch_jump(previous1, previous2) / (max_pitch - min_pitch)
        dico['fret-2'] = mean_fret(previous2) / num_fret
        dico['string-2'] = (mean_string(previous2)) / (num_string - 1)
        dico['pitch-2'] = (mean_midi_pitch(previous2)-min_pitch) / (max_pitch - min_pitch)
    else:
        dico['fret_jump-2'] = 0
        dico['string_jump-2'] = 0
        dico['pitch_jump-2'] = 0
        dico['fret-2'] = 0
        dico['string-2'] = 0
        dico['pitch-2'] = 0
    if next1 is not None:
        dico['pitch_jump+1'] = pitch_jump(next1, current) / (max_pitch - min_pitch)
        dico['fret+1'] = mean_fret(next1) / num_fret
        dico['string+1'] = (mean_string(next1)) / (num_string - 1)
        dico['pitch+1'] = (mean_midi_pitch(next1)-min_pitch) / (max_pitch - min_pitch)
    else:
        dico['pitch_jump+1'] = 0
        dico['fret+1'] = 0
        dico['string+1'] = 0
        dico['pitch+1'] = 0
    if next2 is not None:
        dico['pitch_jump+2'] = pitch_jump(next2, next1) / (max_pitch - min_pitch)
        dico['fret_jump+2'] = fret_jump(next2, next1) / num_fret
        dico['string_jump+2'] = string_jump(next2, next1) / (num_string - 1)
        dico['fret+2'] = mean_fret(next2) / num_fret
        dico['string+2'] = (mean_string(next2)) / (num_string - 1)
        dico['pitch+2'] = (mean_midi_pitch(next2) - min_pitch) / (max_pitch - min_pitch)
    else:
        dico['pitch_jump+2'] = 0
        dico['fret_jump+2'] = 0
        dico['string_jump+2'] = 0
        dico['fret+2'] = 0
        dico['string+2'] = 0
        dico['pitch+2'] = 0
    return dico


def process_stream(stream: m21.stream.Stream, sub_df = None, LEAD_THRESHOLD = 0.5):
    counter = 0
    df = pd.DataFrame(columns=['num_notes',  'mean_pitch', 'duration', 'beat_strength',
                            'fret-1', 'fret-2', 'fret+1', 'fret+2', 'string-1', 'string-2', 'string+1', 'string+2',
                            'pitch-1', 'pitch-2', 'pitch+1', 'pitch+2',
                            'no_n-1', 'longer', 'shorter', 'equal',
                            'no_n-2', 'fret_jump-2', 'string_jump-2', 
                            'no_n+1', 'fret_jump+2', 'string_jump+2',
                            'pitch_jump-1', 'pitch_jump-2', 'pitch_jump+1', 'pitch_jump+2',
                            'accidentals', 'pc_wrt_tonic',
                            'no_n+2', 'bend_class'])
    previous1 = None
    previous2 = None
    next1 = None
    next2 = None
    stream_flat = stream.flatten()
    tied_notes = []
    tied_objects = []
    key = stream_flat.getElementsByClass(m21.key.KeySignature)[0]
    for note_or_rest in list(stream.recurse()):
        if not isinstance(note_or_rest, m21.note.NotRest):
            continue
        #print(note_or_rest)
        #if note_or_rest.duration.isGrace:
        #    print(note_or_rest.getOffsetBySite(stream_flat))
        #    print(stream_flat.getElementBeforeOffset(note_or_rest.getOffsetBySite(stream_flat), [m21.note.NotRest]).getOffsetBySite(stream_flat))
        measure = note_or_rest.getContextByClass(m21.stream.Measure)
        if sub_df is not None:
            meas_num = measure.number
            try:
                if sub_df[sub_df['Mesure'] == meas_num].iloc[0]['rg-estimation'] < LEAD_THRESHOLD:
                    continue
            except IndexError:
                continue
        old_tied_objects = tied_objects
        bend, bend_type, tied_notes, duration, tied_objects = _process_notRest(note_or_rest, tied_notes, return_duration=True, tied_objects=tied_objects)
        #print("bjr")
        #print(bend, bend_type, tied_notes, duration, tied_objects) 
        if bend_type == 'processing':
            continue
        if len(old_tied_objects) >= 1:
            start = old_tied_objects[0]
            end = note_or_rest
        else:
            start = note_or_rest
            end = note_or_rest
        measure_dur = measure.barDuration.quarterLength
        previous1 = stream_flat.getElementBeforeOffset(start.getOffsetBySite(stream_flat), [m21.note.NotRest])
        previous2 = stream_flat.getElementBeforeOffset(previous1.getOffsetBySite(stream_flat), [m21.note.NotRest]) if previous1 is not None else None
        next1 = stream_flat.getElementAfterElement(end, [m21.note.NotRest])
        next2 = stream_flat.getElementAfterElement(next1, [m21.note.NotRest]) if next1 is not None else None
        # must check if previous is None anyway to avoid MissingAttributeError
        if previous1 is None or \
        start.getOffsetBySite(stream_flat) - previous1.getOffsetBySite(stream_flat) - previous1.duration.quarterLength >= measure_dur:
            previous1 = None
            previous2 = None
        if previous2 is None or previous1.getOffsetBySite(stream_flat) - previous2.getOffsetBySite(stream_flat) - previous2.duration.quarterLength >= measure_dur:
            previous2 = None
        if next1 is None or next1.getOffsetBySite(stream_flat) - end.getOffsetBySite(stream_flat) - end.duration.quarterLength >= measure_dur:
            next1 = None
            next2 = None
        if next2 is None or next2.getOffsetBySite(stream_flat) - next1.getOffsetBySite(stream_flat) - next1.duration.quarterLength >= measure_dur:
            next2 = None
        note_or_rest.duration = m21.duration.Duration(quarterLength=duration)
        if note_or_rest.isChord:
            bend_found = False
            for note in note_or_rest.notes:
                if note.bend is not None:
                    bend_found = True
                    new_art = []
                    for a in note.articulations:
                        if not isinstance(a, m21.articulations.FretBend):
                            new_art.append(a)
                    new_art.append(bend)
                    note.articulations = new_art
            if not bend_found:
                note.articulations.append(bend)
        else:
            if note_or_rest.bend is not None:
                new_art = []
                for a in note_or_rest.articulations:
                    if not isinstance(a, m21.articulations.FretBend):
                        new_art.append(a)
                new_art.append(bend)
                note_or_rest.articulations = new_art
            else:
                if bend is not None:
                    note_or_rest.articulations.append(bend)
        bend_class = 0
        bs = beat_strength(start)
        match bend_type:
            case 'regular_bend':
                bend_class = 1
            case 'prebend_no_release':
                bend_class = 2
            case 'prebend_yes_release':
                bend_class = 3
            case 'bend_with_release':
                bend_class = [1,3]
            case _:
                bend_class = 0
        tmp, _ = _bend_remover(note_or_rest)
        if tmp is None:
            current = note_or_rest
            features = make_feat_dictionary(current, 
                    previous1=previous1, previous2=previous2,
                    next1=next1, next2=next2,
                    key=key)
            features['bend_class'] = bend_class
            features['beat_strength'] = bs
            df_feat = pd.DataFrame([features])
        elif isinstance(tmp, list):
            current0 = tmp[0]
            current1 = tmp[1]
            features0 = make_feat_dictionary(current0,
                    previous1=previous1, previous2=previous2,
                    next1=current1, next2=next1,
                    key=key)
            features0['bend_class'] = 1
            features0['beat_strength'] = bs
            df_feat0 = pd.DataFrame([features0])
            features1 = make_feat_dictionary(current1,
                    previous1=current0, previous2=previous1,
                    next1=next1, next2=next2,
                    key=key)
            features1['bend_class'] = 3
            features1['beat_strength'] = bs
            df_feat1 = pd.DataFrame([features1])
            df_feat = pd.concat([df_feat0, df_feat1], ignore_index=True)
            current = current1
        else:
            current = tmp
            features = make_feat_dictionary(current,
                    previous1=previous1, previous2=previous2,
                    next1=next1, next2=next2,
                    key=key)
            features['bend_class'] = bend_class
            features['beat_strength'] = bs
            df_feat = pd.DataFrame([features])
        df = pd.concat([df, df_feat], ignore_index=True)
    return df, counter


def main(parser: ArgumentParser):
    df = pd.DataFrame(columns=['file', 'num_notes', 'mean_pitch',
                            'duration', 'beat_strength', 
                            'no_n-1', 'longer', 'shorter', 'equal',
                            'fret-1', 'fret-2', 'fret+1', 'fret+2', 'string-1', 'string-2', 'string+1', 'string+2',
                            'no_n-2', 'fret_jump-2', 'string_jump-2', 
                            'no_n+1', 'fret_jump+2', 'string_jump+2',
                            'pitch-1', 'pitch-2', 'pitch+1', 'pitch+2',
                            'pitch_jump-1', 'pitch_jump-2', 'pitch_jump+1', 'pitch_jump+2',
                            'accidentals', 'pc_wrt_tonic',
                            'bend_class'])
    args = vars(parser.parse_args())
    total_counter = 0
    DF_STATS = args['rg_estimation']
    if DF_STATS is not None:
        df_stats = pd.read_csv(DF_STATS, index_col=0)
        df_stats['Fichier'] = df_stats['Fichier'].map(lambda x: x.split('.')[0])
    else:
        df_stats = None
    LEAD_THRESHOLD = args['lead_threshold']
    if args['lead_files'] is not None:
        with open(args['lead_files'], 'rb') as f:
            lead_files = pickle.load(f)
    else:
        lead_files = None
    PATH = pathlib.Path(args['source'])
    for file in tqdm(PATH.rglob("*.pickle"), total=2244):
        if lead_files is not None:
            if file.stem not in [name.split('.')[0] for name in lead_files]:
                continue
        stream = m21.converter.thaw(file)
        if df_stats is None:
            df_stream, counter = process_stream(stream)
            total_counter += counter
            df_stream['file'] = file.name
            df = pd.concat([df, df_stream], ignore_index=True)
        else:
            sub_df = df_stats[df_stats['Fichier'] == file.stem][['Mesure', 'rg-estimation']]
            df_stream, counter = process_stream(stream, sub_df=sub_df)
            total_counter += counter
            df_stream['file'] = file.name
            df = pd.concat([df, df_stream], ignore_index=True)
    print("Missed notes because previous is whole rest: ", total_counter)
    df.to_csv(PATH / args['df_name'])



if __name__ == '__main__':
    parser = ArgumentParser(description="Parse some music21 streams and create a Dataframe with features from the notes")
    parser.add_argument('--source', '-s', type=str,
            help="Path to the source directory")
    parser.add_argument('--lead-files', type=str, required=False,
            help="Pickled list if lead files to process")
    parser.add_argument("--df-name", '-n', type=str, default="df.csv",
            help="Name to give to the produced dataframe")
    parser.add_argument("--rg-estimation", type=str, required=False,
            help="path to the .csv file with rg-estimation")
    parser.add_argument("--lead-threshold", default=0.5, type=float,
            help="Threshold to consider a measure to be Lead. 1 means 100% sure, 0 means take everything")
    main(parser)

