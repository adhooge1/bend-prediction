import argparse
import pathlib
from typing import List, Optional, Tuple

# from encoders import RelativeStringFretFullEncoder
import music21 as m21
from music21.interval import Interval
import numpy as np
from numpy.typing import ArrayLike
import warnings



BEND_DICT = {"regular_bend": 0,
             "prebend_no_release": 1,
             "prebend_yes_release": 2,
             "bend_with_release": 3,
             "vibrato_bend": 4}

class BendWarning(Warning):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)
    def __str__(self) -> str:
        return super().__str__()


def _merge_tied_notes_bends(tied_notes: list):
    start_bend = tied_notes[0].bend
    string = tied_notes[0].string
    if start_bend is None:
        if len(tied_notes) > 1:
             start_bend = tied_notes[1].bend
             if start_bend is None:
                 return None
        else:
            return None
    out = m21.articulations.FretBend(preBend=start_bend.preBend, release=start_bend.release)
    out.bendAlter = start_bend.bendAlter
    out.origin_duration = start_bend.origin_duration
    out.middle_duration = start_bend.middle_duration
    out.end_duration = start_bend.end_duration
    for note in tied_notes[1:-1]:
        if string != note.string:
            raise ValueError("Notes are on different strings!")
        bend = note.bend
        if bend is None:
            continue
        if bend.bendAlter != out.bendAlter:
            #TODO: make it clean in m21. See Rory Gallagher for an example
            return "vibrato bend"
        if bend.release is not None:
            out.release = 50
        out.middle_duration += bend.origin_duration + bend.middle_duration + bend.end_duration
    if string != tied_notes[-1].string:
        raise ValueError("Notes are on different strings!")
    end_bend = tied_notes[-1].bend
    if end_bend is None:
        return out
    out.end_duration += end_bend.origin_duration + end_bend.middle_duration + end_bend.end_duration
    if end_bend.release is not None:
        out.release = out.origin_duration + out.middle_duration
    return out


def _get_bend_type_from_FretBend(fretbend) -> str:
    if fretbend is None:
        return 'no_bend'
    if fretbend == "vibrato bend":
        # TODO: make it cleaner
        return "vibrato_bend"
    if fretbend.preBend:
        if fretbend.release is not None:
            return 'prebend_yes_release'
        else:
            return 'prebend_no_release'
    elif fretbend.release is not None:
        return 'bend_with_release'
    else:
        return 'regular_bend'


def count_bend_types(stream: m21.stream.Stream, df_lead = None, filename = None,
        LEAD_THRESHOLD = 0.5) -> Tuple[int, int, int, int, int]:
    """count_bend_types.
    Count regular bends, bends that are pre-bended and bends with release in Stream.

    Args:
        stream (m21.stream.Stream): stream
        df_lead (Optional): dataframe that tells wether the measures of each song are lead guitar or not
        filename (Optional): name of the track, necessary to access correct row in df_lead

    Returns:
        Tuple[int, int, int, int, int]: regular_bends, prebend with no release, prebends 
        with release, bends with release, vibrato bends
    """
    regular_bends = 0
    prebend_no_release = 0
    prebend_yes_release = 0
    bend_with_release = 0
    vibrato_bend = 0
    other_ties_counter = 0
    tied_notes = []
    if df_lead is not None:
        sub_df = df_lead[df_lead["Fichier"].apply(lambda x: x.split('.')[0]) == filename][['Mesure', 'rg-estimation']]
    for not_rest in stream.flatten().notes:
        if df_lead is not None:
            meas_num = not_rest.getContextByClass(m21.stream.Measure).number
            try:
                if sub_df[sub_df['Mesure'] == meas_num].iloc[0]['rg-estimation'] < LEAD_THRESHOLD:
                    continue
            except IndexError:
                continue
        answer = None
        if not_rest.isChord:
            bend_detected = False
            for note in not_rest.notes:
                if bend_detected:
                    break
                if note.bend is not None:
                    bend_detected = True
                    if not_rest.tie is not None:
                        if not_rest.tie.type == 'start':
                            if len(tied_notes) > 0:
                                # Some bend was tied to a note without bend, the previous one is kept as is
                                bend = _merge_tied_notes_bends(tied_notes)
                                answer = _get_bend_type_from_FretBend(bend)
                                tied_notes = []
                                tied_notes.append(note)
                            else:
                                tied_notes = []
                                tied_notes.append(note)
                                continue
                        elif not_rest.tie.type == 'continue':
                            tied_notes.append(note)
                            continue
                        elif not_rest.tie.type == 'stop':
                            tied_notes.append(note)
                            bend = _merge_tied_notes_bends(tied_notes)
                            answer = _get_bend_type_from_FretBend(bend)
                            tied_notes = []
                        else:
                            other_ties_counter += 1
                    else:
                        answer = _get_bend_type_from_FretBend(note.bend)
        else:
            if not_rest.bend is not None:
                if not_rest.tie is not None:
                    if not_rest.tie.type == 'start':
                        if len(tied_notes) > 0:
                            # Some bend was tied to a note without bend, the previous one is kept as is
                            print("weird tied bend")
                            bend = _merge_tied_notes_bends(tied_notes)
                            answer = _get_bend_type_from_FretBend(bend)
                            tied_notes = []
                            tied_notes.append(not_rest)
                        else:
                            tied_notes = []
                            tied_notes.append(not_rest)
                            continue
                    elif not_rest.tie.type == 'continue':
                        tied_notes.append(not_rest)
                        continue
                    elif not_rest.tie.type == 'stop':
                        tied_notes.append(not_rest)
                        bend = _merge_tied_notes_bends(tied_notes)
                        answer = _get_bend_type_from_FretBend(bend)
                        tied_notes = []
                    else:
                        other_ties_counter += 1
                else:
                    answer = _get_bend_type_from_FretBend(not_rest.bend)
        if answer is not None:
            match answer:
                case 'regular_bend':
                    regular_bends += 1
                case 'prebend_no_release':
                    prebend_no_release += 1
                case 'prebend_yes_release':
                    prebend_yes_release += 1
                case 'bend_with_release':
                    bend_with_release += 1
                case 'vibrato_bend':
                    vibrato_bend += 1
                case _:
                    raise ValueError("Unknown answer", answer)
    if other_ties_counter > 0:
        print("Other ties on bend: ", other_ties_counter)
    return regular_bends, prebend_no_release, prebend_yes_release, bend_with_release, vibrato_bend






def _count_chords_vs_notes(stream: m21.stream.Stream) -> Tuple[int, int]:
    """_count_chords_vs_notes.
    Count number of chords and number of notes in input stream

    Args:
        stream (m21.stream.Stream): stream

    Returns:
        Tuple[int, int]: number of chords, number of single notes
    """
    chords = 0
    notes = 0
    for not_rest in stream.flatten().notes:
        if not_rest.isChord:
            chords += 1
        else:
            notes += 1
    return chords, notes


def _number_of_notes(not_rest: m21.note.NotRest) -> int:
    if not_rest.isChord:
        return len(not_rest.notes)
    else:
        return 1


def count_simultaneous_notes(stream: m21.stream.Stream) -> List[int]:
    """count_simultaneous_notes.
    Simple helper function that iterates through a stream and count the number of single notes.
    In that case, a 1 thus means a single note.

    Args:
        stream (m21.stream.Stream): stream

    Returns:
        List[int]: list of the number of simultaneous notes.
    """
    out = []
    for not_rest in stream.flatten().notes:
        out.append(_number_of_notes(not_rest))
    return out



def chord_size_with_bend(stream: m21.stream.Stream, df_lead = None,
                        filename = None,
                        LEAD_THRESHOLD = 0.5) -> dict:
    """chord_size_with_bend.
    """
    out = {key: [] for key in BEND_DICT.keys()}
    out['no_bend'] = []
    tied_notes = []
    if df_lead is not None:
        sub_df = df_lead[df_lead["Fichier"].apply(lambda x: x.split('.')[0]) == filename][['Mesure', 'rg-estimation']]
    for not_rest in stream.flatten().notes:
        if df_lead is not None:
            meas_num = not_rest.getContextByClass(m21.stream.Measure).number
            try:
                if sub_df[sub_df['Mesure'] == meas_num].iloc[0]['rg-estimation'] < LEAD_THRESHOLD:
                    continue
            except IndexError:
                continue
        _, bend_type, tied_notes = _process_notRest(not_rest, tied_notes)
        if bend_type == 'processing':
            continue
        out[bend_type].append(len(not_rest.notes) if not_rest.isChord else 1)
    return out


def count_bended_chords_vs_bended_notes(stream: m21.stream.Stream) -> Tuple[int, int]:
    """count_bended_chords_vs_bended_notes.
    Count number of chords that contains a bend and number of bended single notes.

    Args:
        stream (m21.stream.Stream): stream

    Returns:
        Tuple[int, int]: number of chords with bend, number of bended single notes
    """
    bend_in_chords = 0
    bended_notes = 0
    for not_rest in stream.flatten().notes:
        if not_rest.isChord:
            for note in not_rest.notes:
                if note.bend is not None:
                    bend_in_chords += 1
        else:
            if not_rest.bend is not None:
                bended_notes += 1
    return bend_in_chords, bended_notes



def relative_heatmap(stream: m21.stream.Stream, handspan: int = 5) -> ArrayLike:
    heatmap = np.zeros((6, handspan + 1))
    encoder = RelativeStringFretFullEncoder([40, 45, 50, 55, 59, 64], 24, 6, handspan)
    array = encoder.getManyHotFromStream(stream, 0.25, False)
    for i, vect in enumerate(array):
        if 1 in vect[encoder.numFret + 1:encoder.numFret+1+encoder.numString*(handspan+1)]:
            if not(1 in vect[encoder.bendIndices[0]:encoder.bendIndices[-1] + 5]):
                string = np.nonzero(vect[encoder.numFret + 1:encoder.numFret+1+encoder.numString*(handspan+1)] == 1)[0][0]
                string = int(string / (handspan+1))
                try:
                    relative_fret = np.nonzero(vect[encoder.numFret + 1 + (string * (handspan+1)):encoder.numFret + 1 + ((string+1) * (handspan+1))]==1)[0][0]
                except IndexError:
                    print("Timeslice is: ", i)
                    print("Error while processing vector")
                    print("String is: ", string)
                    print("Ones are here: ", np.nonzero(vect))
                    print(np.nonzero(vect[encoder.numFret + 1 + (string * (handspan+1)):encoder.numFret + 1 + ((string+1) * (handspan+1))]==1))
                    print("vector was: ", vect)
                    raise ValueError
                    continue
                heatmap[string, relative_fret] += 1
    return heatmap


def relative_heatmap_bends(stream: m21.stream.Stream, handspan: int = 5) -> ArrayLike:
    heatmap = np.zeros((6, handspan + 1))
    heatmap_0st = np.zeros((6, handspan + 1))
    heatmap_1st = np.zeros((6, handspan + 1))
    heatmap_2st = np.zeros((6, handspan + 1))
    heatmap_3st = np.zeros((6, handspan + 1))
    heatmap_4st = np.zeros((6, handspan + 1))
    encoder = RelativeStringFretFullEncoder([40, 45, 50, 55, 59, 64], 24, 6, handspan)
    array = encoder.getManyHotFromStream(stream, 0.25, False)
    for i, vect in enumerate(array):
        if 1 in vect[encoder.bendIndices[0]:encoder.bendIndices[-1] + 5]:
            string = np.nonzero(vect[encoder.bendIndices[0]:encoder.bendIndices[-1] + 5] == 1)[0][0]
            string = int(string / 5)
            try:
                relative_fret = np.nonzero(vect[encoder.numFret + 1 + (string * (handspan+1)):encoder.numFret + 1 + ((string+1) * (handspan+1))]==1)[0][0]
            except IndexError:
                print("Timeslice is: ", i)
                print("Error while processing vector")
                print("String is: ", string)
                print("Ones are here: ", np.nonzero(vect))
                print(np.nonzero(vect[encoder.numFret + 1 + (string * (handspan+1)):encoder.numFret + 1 + ((string+1) * (handspan+1))]==1))
                print("vector was: ", vect)
                raise ValueError
                continue
            heatmap[string, relative_fret] += 1
            if vect[encoder.bendIndices[string]] == 1:
                heatmap_0st[string, relative_fret] += 1
            elif vect[encoder.bendIndices[string] + 1] == 1:
                heatmap_1st[string, relative_fret] += 1
            elif vect[encoder.bendIndices[string] + 2] == 1:
                heatmap_2st[string, relative_fret] += 1
            elif vect[encoder.bendIndices[string] + 3] == 1:
                heatmap_3st[string, relative_fret] += 1
            elif vect[encoder.bendIndices[string] + 4] == 1:
                heatmap_4st[string, relative_fret] += 1
    return heatmap, heatmap_0st, heatmap_1st, heatmap_2st, heatmap_3st, heatmap_4st


def _get_bended_notes_from_chord(chord: m21.chord.Chord) -> List[m21.note.Note]:
    out = []
    for note in chord.notes:
        if note.bend is not None:
            out.append(note)
    return out


def heatmap_bends(stream: m21.stream.Stream) -> dict:
    """heatmap_bends. Detect any bends present in the notes of the stream
    """
    out = {key: np.zeros((6,25)) for key in BEND_DICT.keys()}
    out['no_bend'] = np.zeros((6,25))
    tied_notes = []
    for not_rest in stream.flatten().notes:
        _, bend_type, tied_notes = _process_notRest(not_rest, tied_notes)
        if bend_type == 'processing':
            continue
        if not_rest.isChord:
            if bend_type != "no_bend":
                for note in _get_bended_notes_from_chord(not_rest):
                    if note.fret.number >= 25:
                        # discard non standard fretboards
                        continue
                    out[bend_type][note.string.number-1, note.fret.number] += 1
            else:
                for note in not_rest.notes:
                    # if there is no bend, each note of the chord is shown on the heatmap
                    if note.fret.number >= 25:
                        # discard non standard fretboards
                        continue
                    out[bend_type][note.string.number-1, note.fret.number] += 1
        else:
            if not_rest.fret.number >= 25:
                continue
            out[bend_type][not_rest.string.number-1, not_rest.fret.number] += 1
    return out


def _sum_duration(tied_notes: list):
    """_sum_duration.
    Helper function that sums the duration in quarter length of the notes in the list
    provided as input.

    Args:
        tied_notes (list): tied_notes
    Returns:
        float: cumul duration in quarterLength
    """
    res = 0
    for note in tied_notes:
        res += note.quarterLength
    return res


def _process_notRest(not_rest: m21.note.NotRest, tied_notes: list, return_duration: bool = False, 
        tied_objects = None):
    bend_type = "no_bend"
    bend = None
    duration = None
    if not_rest.isChord:
        bend_detected = False
        for note in not_rest.notes:
            if bend_detected:
                break
            if note.bend is not None:
                bend_detected = True
                if not_rest.tie is not None:
                    if not_rest.tie.type == 'start':
                        if len(tied_notes) > 0:
                            # Some bend was tied to a note without bend, the previous one is kept as is
                            bend = _merge_tied_notes_bends(tied_notes)
                            bend_type = _get_bend_type_from_FretBend(bend)
                            if return_duration:
                                duration = _sum_duration(tied_notes)
                            tied_notes = []
                            tied_objects = []
                            tied_notes.append(note)
                            tied_objects.append(not_rest)
                        else:
                            tied_notes = []
                            tied_objects = []
                            tied_notes.append(note)
                            tied_objects.append(not_rest)
                            bend_type = 'processing'
                            continue
                    elif not_rest.tie.type == 'continue':
                        tied_notes.append(note)
                        tied_objects.append(not_rest)
                        bend_type = 'processing'
                        continue
                    elif not_rest.tie.type == 'stop':
                        tied_notes.append(note)
                        tied_objects.append(not_rest)
                        bend = _merge_tied_notes_bends(tied_notes)
                        bend_type = _get_bend_type_from_FretBend(bend)
                        if return_duration:
                            duration = _sum_duration(tied_notes)
                        tied_notes = []
                        tied_objects = []
                    else:
                        # let-ring tie
                        bend = note.bend
                        bend_type = _get_bend_type_from_FretBend(note.bend)
                else:
                    bend = note.bend
                    bend_type = _get_bend_type_from_FretBend(note.bend)
        if not bend_detected:
            if not_rest.tie is not None:
                if not_rest.tie.type == 'start':
                    if len(tied_notes) > 0:
                        # Some bend was tied to a note without bend, the previous one is kept as is
                        bend = None
                        bend_type = 'no_bend'
                        if return_duration:
                            duration = _sum_duration(tied_notes)
                        tied_notes = []
                        tied_objects = []
                        tied_notes.append(note)
                        tied_objects.append(not_rest)
                    else:
                        tied_notes = []
                        tied_objects = []
                        tied_notes.append(note)
                        tied_objects.append(not_rest)
                        bend_type = 'processing'
                elif not_rest.tie.type == 'continue':
                    tied_notes.append(note)
                    tied_objects.append(not_rest)
                    bend_type = 'processing'
                elif not_rest.tie.type == 'stop':
                    tied_notes.append(note)
                    tied_objects.append(not_rest)
                    bend = _merge_tied_notes_bends(tied_notes)
                    bend_type = _get_bend_type_from_FretBend(bend)
                    if return_duration:
                        duration = _sum_duration(tied_notes)
                    tied_notes = []
                    tied_objects = []
                else:
                    # let-ring tie
                    bend = None
                    bend_type = 'no_bend'
    else:
        if not_rest.bend is not None:
            if not_rest.tie is not None:
                if not_rest.tie.type == 'start':
                    if len(tied_notes) > 0:
                        # Some bend was tied to a note without bend, the previous one is kept as is
                        bend = _merge_tied_notes_bends(tied_notes)
                        bend_type = _get_bend_type_from_FretBend(bend)
                        if return_duration:
                            duration = _sum_duration(tied_notes)
                        tied_notes = []
                        tied_objects = []
                        tied_notes.append(not_rest)
                        tied_objects.append(not_rest)
                    else:
                        tied_notes = []
                        tied_objects = []
                        tied_notes.append(not_rest)
                        tied_objects.append(not_rest)
                        bend_type = 'processing'
                elif not_rest.tie.type == 'continue':
                    tied_notes.append(not_rest)
                    tied_objects.append(not_rest)
                    bend_type = 'processing'
                elif not_rest.tie.type == 'stop':
                    tied_notes.append(not_rest)
                    tied_objects.append(not_rest)
                    bend = _merge_tied_notes_bends(tied_notes)
                    bend_type = _get_bend_type_from_FretBend(bend)
                    if return_duration:
                        duration = _sum_duration(tied_notes)
                    tied_notes = []
                    tied_objects = []
                else:
                    # let-ring tie
                    bend = not_rest.bend
                    bend_type = _get_bend_type_from_FretBend(not_rest.bend)
            else:
                bend = not_rest.bend
                bend_type = _get_bend_type_from_FretBend(not_rest.bend)
        else:
            if not_rest.tie is not None:
                if not_rest.tie.type == 'start':
                    if len(tied_notes) > 0:
                        bend = None
                        bend_type = 'no_bend'
                        if return_duration:
                            duration = _sum_duration(tied_notes)
                        tied_notes = []
                        tied_objects = []
                        tied_notes.append(not_rest)
                        tied_objects.append(not_rest)
                    else:
                        tied_notes = []
                        tied_objects = []
                        tied_notes.append(not_rest)
                        tied_objects.append(not_rest)
                        bend_type = 'processing'
                elif not_rest.tie.type == 'continue':
                    tied_notes.append(not_rest)
                    tied_objects.append(not_rest)
                    bend_type = 'processing'
                elif not_rest.tie.type == 'stop':
                    tied_notes.append(not_rest)
                    tied_objects.append(not_rest)
                    bend = _merge_tied_notes_bends(tied_notes)
                    bend_type = _get_bend_type_from_FretBend(bend)
                    if return_duration:
                        duration = _sum_duration(tied_notes)
                    tied_notes = []
                    tied_objects = []
                else:
                    # let-ring tie
                    bend = None
                    bend_type = 'no_bend'
            else:
                bend = None
                bend_type = 'no_bend'
    if return_duration:
        if duration is None:
            duration = not_rest.quarterLength
        return bend, bend_type, tied_notes, duration, tied_objects
    return bend, bend_type, tied_notes



def bend_alter_stats(stream: m21.stream.Stream) -> dict:
    """bend_alter_stats.
    """
    out = {key: [] for key in BEND_DICT.keys()}
    tied_notes = []
    for not_rest in stream.flatten().notes:
        bend, bend_type, tied_notes = _process_notRest(not_rest, tied_notes)
        if bend_type == 'no_bend' or bend_type == 'processing':
            continue
        if bend_type == 'vibrato_bend':
            continue
        out[bend_type].append(round(bend.bendAlter.semitones))
    return out


def duration_bend_stats(stream: m21.stream.Stream, absolute: bool = False,
        no_tuplets: bool = False, bin_size: float = 0.2) -> dict:
    """duration_bend_stats.
    """
    # TODO: add no_tuplets version
    if absolute:
        try:
            mark = stream.getElementsByClass(m21.tempo.MetronomeMark)
            mark = mark[0]
            bpm = mark.number / mark.referent.quarterLength
        except:
            print("Impossible de récupérer le bpm")
            raise ValueError    # TODO mettre une erreur plus détaillée
    out = {key: [] for key in BEND_DICT.keys()}
    out['no_bend'] = []
    tied_notes = []
    for not_rest in stream.flatten().notes:
        _, bend_type, tied_notes, duration = _process_notRest(not_rest, tied_notes, return_duration=True)
        if bend_type == 'processing':
            continue
        if absolute:
            out[bend_type].append(duration*60/bpm)
        else:
            out[bend_type].append(duration)
    return out

def duration_nobend_stats(stream: m21.stream.Stream, absolute: bool = False,
        no_tuplets: bool = False, bin_size: float = 0.2) -> dict:
    """duration_nobend_stats.
    Compute the number of not bended notes sorted by duration.

    Args:
        stream (m21.stream.Stream): stream to analyse;
        absolute (bool): should the duration be absolute or not (uses tempo information);
        no_tuplets (bool): if set to True, rounds tuplets to the nearest quarter note fraction. Default is False.
        bin_size (float): Ignored if absolute is False. Size of the bins to gather
            duration values, first bin will be centered on bin_size/2.
            Default value is 0.2.

    Returns:
        dict: number of not bended notes per note duration.
    """
    # TODO: add absolute version
    # TODO: add no_tuplets version
    out = {}
    datapoints = []
    if absolute:
        try:
            mark = stream.getElementsByClass(m21.tempo.MetronomeMark)
            mark = mark[0]
            bpm = mark.number / mark.referent.quarterLength
        except:
            print("Impossible de récupérer le bpm")
            raise ValueError    # TODO mettre une erreur plus détaillée
    for not_rest in stream.flatten().notes:
        if not_rest.isChord:
            for note in not_rest.notes:
                if note.bend is None:
                    if absolute:
                        dur = note.duration.quarterLength*60/bpm
                        datapoints.append(dur)
                        dur = round((dur//bin_size)*bin_size + bin_size/2, 4)
                    else:
                        dur = note.duration.fullName
                        datapoints.append(dur)
                    if dur not in out.keys():
                        out[dur] = 1
                    else:
                        out[dur] += 1
        else:
            if not_rest.bend is None:
                if absolute:
                    dur = not_rest.duration.quarterLength*60/bpm
                    datapoints.append(dur)
                    dur = round((dur//bin_size)*bin_size + bin_size/2, 4)
                else:
                    dur = not_rest.duration.fullName
                    datapoints.append(dur)
                if dur not in out.keys():
                    out[dur] = 1
                else:
                    out[dur] += 1
    return out, datapoints

def _merge_duration_dict(dict1: dict, dict2: dict) -> dict:
    """_merge_duration_dict.
    Combine two dictionaries of duration according to the following rules:
    - if a key is present in only one of the input dict, it is appended to the output with its value;
    - if a key is present in both dicts, values are summed.

    Args:
        dict1 (dict): dict1
        dict2 (dict): dict2

    Returns:
        dict:
    """
    out = dict1 | dict2 
    # Pipe operator only keep value of righthand dict, so manual correction
    for key in out.keys():
        if key in dict1.keys():
            out[key] += dict1[key]
    return out


def interval_with_previous_note(stream: m21.stream.Stream,
        replace_with_dest: bool = False):
    """interval_with_previous_note.
    Return list of intervals of a bended note wrt the previous_note. If a bend is played after a rest, the interval is undefined.

    Args:
        stream (m21.stream.Stream): stream
        replace_with_dest (bool): Should the bended note pitch value be considered as the destination of the bend? Default is False. It is expected that keeping original pitch will give information regarding biomechanical constraints while replacing with destination would be related to musical information.

    Returns:
    """
    out = {key: [] for key in BEND_DICT.keys()}
    out['no_bend'] = []
    tied_notes = []
    undefined = 0
    previous = None
    for item in stream.flatten():
        if isinstance(item, m21.note.Rest):
            # There is no "previous note" when the note is after a rest
            # TODO: add an argument to disable that to see the difference
            previous = None
        elif isinstance(item, m21.note.NotRest):
            if previous is None:
                undefined += 1
                previous = item
                continue
            else:
                _, bend_type, tied_notes = _process_notRest(item, tied_notes)
                if bend_type == 'processing':
                    continue
                else:
                    interval = _get_interval_with_previous_note(item, previous, replace_with_dest)
                if interval is None:
                    undefined += 1
                else:
                    out[bend_type].append(interval.semitones)
    return out
    out = []
    out_no_bend = []
    undefined = 0
    previous = None
    for item in stream.flatten():
        if isinstance(item, m21.note.NotRest):
            if previous is None:
                undefined += 1
                if not isinstance(item, m21.note.Rest):
                    previous=item
                continue
            if item.isChord:
                #TODO
                pass
            else:
                if previous.isChord:
                    previous_note = _get_note_on_closest_string(previous, item)
                else:
                    previous_note = previous
                interval = m21.interval.Interval(previous_note, item)
                if item.bend is not None:
                    if replace_with_dest:
                        out.append(interval.semitones + item.bend.bendAlter.semitones)
                    else:
                        out.append(interval.semitones)
                else:
                    out_no_bend.append(interval.semitones)
            previous = item
        elif isinstance(item, m21.note.Rest):
            previous = None
    return out, out_no_bend, undefined


def _get_interval_with_previous_note(item, previous, replace_with_dest):
    previous_note, note = None, None
    if item.isChord and not previous.isChord:
        note = _get_note_on_closest_string(item, previous)
        previous_note = previous
    elif previous.isChord and not item.isChord:
        previous_note = _get_note_on_closest_string(previous, item)
        note = item
    elif previous.isChord and item.isChord:
        bended_notes = _get_bended_notes_from_chord(item)
        if len(bended_notes) == 0:
            bended_notes = _get_bended_notes_from_chord(previous)
            if len(bended_notes) == 0:
                return None
            elif len(bended_notes) > 1:
                warnings.warn("Several bends in chord.", BendWarning)
            else:
                previous_note = bended_notes[0]
                note = _get_note_on_closest_string(item, previous_note)
        elif len(bended_notes) > 1:
            warnings.warn("Several bends in chord.", BendWarning)
        else:
            note = bended_notes[0]
            previous_note = _get_note_on_closest_string(previous, note)
    else:
        previous_note = previous
        note = item
    if not replace_with_dest:
        return m21.interval.Interval(previous_note, note)
    elif previous_note.bend is not None:
        tmp = m21.common.misc.defaultDeepCopy(previous_note)
        tmp.transpose(tmp.bend.bendAlter, inplace=True)
        return m21.interval.Interval(tmp, note)
    elif note.bend is not None:
        tmp = m21.common.misc.defaultDeepCopy(note)
        tmp.transpose(tmp.bend.bendAlter, inplace=True)
        return m21.interval.Interval(previous_note, tmp)
    else:
        return m21.interval.Interval(previous_note, note)

        




def string_before_bend(stream: m21.stream.Stream, absolute: bool = True,
        num_strings: int = 6) -> Tuple[List[int], List[int], int]:
    """string_before_bend.
    Compute number of notes on a given string when the next note is bended.

    Args:
        stream (m21.stream.Stream): stream to analyze;
        absolute (bool): absolute computation. Default is True. If True, the index in list is the actual number of the string, 0 for the highest and n-1 for the lowest. Else, computation is relative so output list is always odd and count the notes relatively to the previous string e.g. [-2, -1, 0, +1, +2].
            In that case, positive value means that the bend is played on a lower string wrt the previous note and vice-versa.
        num_strings (int): num_strings of considered stream. Default is 6.

    Returns:
        List[int]: number of occurences per string, relatively or absolutely.
    """
    if absolute:
        out = [0] * num_strings
        out_no_bend = [0] * num_strings
    else:
        out = [0]
        out_no_bend = [0]
    undefined = 0
    previous = None
    for item in stream.flatten():
        if isinstance(item, m21.note.NotRest):
            if previous is None:
                undefined += 1
                if not isinstance(item, m21.note.Rest):
                    previous = item
                continue
            if item.isChord:
                #TODO bends in chords are ignored for now
                pass
            else:
                if previous.isChord:
                    previous_note = _get_note_on_closest_string(previous, item)
                else:
                    previous_note = previous
                if absolute:
                    if item.bend is not None:
                        out[previous_note.string.number - 1] += 1 
                    else:
                        out_no_bend[previous_note.string.number - 1] += 1
                else:
                    step = item.string.number - previous_note.string.number
                    if item.bend is not None:
                        while len(out) < (abs(step*2) + 1):
                            out.insert(0, 0)
                            out.append(0)
                        out[step + len(out)//2] += 1
                    else:
                        while len(out_no_bend) < (abs(step*2) + 1):
                            out_no_bend.insert(0, 0)
                            out_no_bend.append(0)
                        out_no_bend[step + len(out_no_bend)//2] += 1
            previous = item
        elif isinstance(item, m21.note.Rest):
            previous = None
    return out, out_no_bend, undefined

def _get_note_on_closest_string(chord, note) -> m21.note.Note:
    """_get_note_on_closest_string.
    Utility function that returns the note from chord that is closest to note, stringwise.

    Args:
        chord:
        note:

    Returns:
        m21.note.Note:
    """
    current_string = note.string.number
    dist = None
    closest_note = None
    for chord_note in chord.notes:
        if dist is None or abs(chord_note.string.number - current_string) < dist:
            closest_note = chord_note
            dist = abs(chord_note.string.number - current_string)
    return closest_note


def duration_diff_bend_wrt_previous(stream: m21.stream.Stream) -> dict:
    """duration_diff_bend_wrt_previous.
    """
    out = {key: [] for key in BEND_DICT.keys()}
    out['no_bend'] = []
    tied_notes = []
    undefined = 0
    previous = None
    duration = None
    for item in stream.flatten():
        if isinstance(item, m21.note.Rest):
            # There is no "previous note" when the note is after a rest
            # TODO: add an argument to disable that to see the difference
            previous = None
            duration = None
        elif isinstance(item, m21.note.NotRest):
            if previous is None:
                undefined += 1
                previous = item
                duration = previous.quarterLength
                continue
            else:
                previous_duration = duration
                _, bend_type, tied_notes, duration = _process_notRest(item, tied_notes, return_duration=True)
                if bend_type == 'processing':
                    continue
                else:
                    duration_diff = duration - previous_duration
                out[bend_type].append(duration_diff)
    return out
    output = []
    output_no_bend = []
    previous = None
    for item in stream.flatten():
        if previous is not None and isinstance(item, m21.note.NotRest):
            if item.isChord:
                #TODO bends in chords are ignored for now
                pass
            else:
                if previous.isChord:
                    previous_note = _get_note_on_closest_string(previous, item)
                else:
                    previous_note = previous
                if item.bend is not None:
                    output.append(item.duration.quarterLength - previous_note.duration.quarterLength)
                else:
                    output_no_bend.append(item.duration.quarterLength - previous_note.duration.quarterLength)
        if isinstance(item, m21.note.NotRest):
            previous = item
        elif isinstance(item, m21.note.Rest):
            previous = None
    return output, output_no_bend


def get_bend_beat(stream: m21.stream.Stream) -> List[int]:
    """get_bend_beat.
    or a TimeSignature different than 4/4 are ignored.
    Also returns list of beats of non-bended notes for comparison

    Args:
        stream (m21.stream.Stream): stream

    Returns:
        List[int]:
    """
    out = []
    out_no_bend = []
    stream_flat = stream.flatten()
    time_sig = stream_flat.getTimeSignatures()
    for ts in time_sig:
        if ts != m21.meter.TimeSignature("4/4"):
            warnings.warn("Song not in 4/4... aborting.", UserWarning)
            return out
    for not_rest in stream_flat.notes:
        if not_rest.isChord:
            bend_detected = False
            for note in not_rest.notes:
                if note.bend is not None and not bend_detected:
                    out.append(not_rest.beat)
                    bend_detected = True
            if not bend_detected:
                out_no_bend.append(not_rest.beat)
        else:
            if not_rest.bend is not None:
                out.append(not_rest.beat)
            else:
                out_no_bend.append(not_rest.beat)
    return out, out_no_bend
        
