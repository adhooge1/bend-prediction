def add_bend_annotations(stream, prediction):
    pred_counter = 0
    for note in stream.flatten().notes:
        if note.tie is not None and note.tie.type in ['continue', 'stop']:
            continue
        if prediction[pred_counter] != 0:
            match prediction[pred_counter]:
                case 1:
                    label = "Up"
                case 2:
                    label = "Held"
                case 3:
                    label = "Down"
            note.addLyric(label)
        pred_counter += 1
    return stream
