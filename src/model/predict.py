import pathlib
import pandas as pd
from joblib import load
from argparse import ArgumentParser
from data.parserGP import ParserGP
from util.feature_extractor import process_stream
from util.add_bend_annotations import add_bend_annotations

parser = ArgumentParser(description="Use the provided input model and score and predict how likely notes are to be bent")

parser.add_argument('-m', '--model', type=str,
        default="../data/trained_models/pretrained-paper.joblib",
        help="Path to a pretrained model.")
parser.add_argument('-s', '--source', type=str,
        default="../data/chopin.gpif",
        help="Path to a .gpif file to analyze or .csv of already analyzed features.")

args = parser.parse_args()

model = load(args.model)
sourcepath = pathlib.Path(args.source)

if sourcepath.suffix == '.gpif':
    stream = ParserGP().parseFile(sourcepath)
    feat, _ = process_stream(stream)
elif sourcepath.suffix == '.csv':
    feat = pd.read_csv(sourcepath)
    stream = None
else:
    raise TypeError("Unknown file type. This script only supports .gpif or .csv files.")

bend_class = feat[['bend_class']]
feat = feat.drop(columns=['bend_class'])

# Reorder features if necessary
feat = feat[model.feature_names_in_]

pred = model.predict(feat)
print(pred)

if stream is not None:
    stream = add_bend_annotations(stream, pred)
    stream.show('musicxml.png')
