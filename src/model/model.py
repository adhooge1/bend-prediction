from joblib import dump
from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import pathlib
from sklearn.model_selection import train_test_split, StratifiedGroupKFold
from sklearn import tree
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression, Perceptron
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.metrics import confusion_matrix, f1_score, top_k_accuracy_score
from sklearn.inspection import permutation_importance
from sklearn.dummy import DummyClassifier

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


SMALL_SIZE = 14
MEDIUM_SIZE = 16
BIGGER_SIZE = 18

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

def make_confusion_matrix(cf,
                          group_names=None,
                          categories='auto',
                          count=True,
                          percent=True,
                          cbar=True,
                          xyticks=True,
                          xyplotlabels=True,
                          color_by_row=True,
                          sum_stats=True,
                          figsize=None,
                          cmap='Oranges',
                          title=None):
    '''
    This function will make a pretty plot of an sklearn Confusion Matrix cm using a Seaborn heatmap visualization.
    Arguments
    ---------
    cf:            confusion matrix to be passed in
    group_names:   List of strings that represent the labels row by row to be shown in each square.
    categories:    List of strings containing the categories to be displayed on the x,y axis. Default is 'auto'
    count:         If True, show the raw number in the confusion matrix. Default is True.
    normalize:     If True, show the proportions for each category. Default is True.
    cbar:          If True, show the color bar. The cbar values are based off the values in the confusion matrix.
                   Default is True.
    xyticks:       If True, show x and y ticks. Default is True.
    xyplotlabels:  If True, show 'True Label' and 'Predicted Label' on the figure. Default is True.
    sum_stats:     If True, display summary statistics below the figure. Default is True.
    figsize:       Tuple representing the figure size. Default will be the matplotlib rcParams value.
    cmap:          Colormap of the values displayed from matplotlib.pyplot.cm. Default is 'Blues'
                   See http://matplotlib.org/examples/color/colormaps_reference.html
                   
    title:         Title for the heatmap. Default is None.
    '''


    # CODE TO GENERATE TEXT INSIDE EACH SQUARE
    blanks = ['' for i in range(cf.size)]

    if group_names and len(group_names)==cf.size:
        group_labels = ["{}\n".format(value) for value in group_names]
    else:
        group_labels = blanks

    if count:
        group_counts = ["{0:0.0f}\n".format(value) for value in cf.flatten()]
    else:
        group_counts = blanks

    if percent:
        group_percentages = []
        ratio = []
        for row in cf:
            for value in row:
                group_percentages.append("{0:2.0%}".format(value/np.sum(row)) if value/np.sum(row)>0.01 else "< 1%") 
                ratio.append(value/np.sum(row))
        #group_percentages = ["{0:.2%}".format(value/np.sum(row)) for value in row for row in cf]
    else:
        group_percentages = blanks

    box_labels = [f"{v1}{v2}{v3}".strip() for v1, v2, v3 in zip(group_labels,group_counts,group_percentages)]
    box_labels = np.asarray(box_labels).reshape(cf.shape[0],cf.shape[1])
    if color_by_row:
        cf = np.asarray(np.array(ratio).reshape(cf.shape[0],cf.shape[1]))


    # CODE TO GENERATE SUMMARY STATISTICS & TEXT FOR SUMMARY STATS
    if sum_stats:
        #Accuracy is sum of diagonal divided by total observations
        accuracy  = np.trace(cf) / float(np.sum(cf))

        #if it is a binary confusion matrix, show some more stats
        if len(cf)==2:
            #Metrics for Binary Confusion Matrices
            precision = cf[1,1] / sum(cf[:,1])
            recall    = cf[1,1] / sum(cf[1,:])
            f1_score  = 2*precision*recall / (precision + recall)
            print("Accuracy: ", accuracy)
            print("Precision: ", precision)
            print("Recall: ", recall)
            print("FScore: ", f1_score)
            stats_text = "\n\nAccuracy={:0.3f}\nPrecision={:0.3f}\nRecall={:0.3f}\nF1 Score={:0.3f}".format(
                accuracy,precision,recall,f1_score)
        else:
            stats_text = "\n\nAccuracy={:0.3f}".format(accuracy)
    else:
        stats_text = ""


    # SET FIGURE PARAMETERS ACCORDING TO OTHER ARGUMENTS
    if figsize==None:
        #Get default figure size if not set
        figsize = plt.rcParams.get('figure.figsize')

    if xyticks==False:
        #Do not show categories if xyticks is False
        categories=False
    if group_names is None:
        group_names='auto'


    # MAKE THE HEATMAP VISUALIZATION
    plt.figure(figsize=figsize)
    sns.heatmap(cf,annot=box_labels,fmt="",cmap=cmap,cbar=cbar,xticklabels=group_names,yticklabels=group_names)
    plt.yticks(rotation=0)

    if xyplotlabels:
        plt.ylabel('True label')
        plt.xlabel('Predicted label' + stats_text)
    else:
        plt.xlabel(stats_text)
    
    if title:
        plt.title(title)

DATAPATH = pathlib.Path("../data/feat_df.csv")
class_names = ['no_bend', 'up', 'held', 'down']
class_names_latex = [r'$\varnothing$', r'$\uparrow$', r'$\rightarrow$', r'$\downarrow$']
df = pd.read_csv(DATAPATH, index_col=0)
weights = {0:0.1,1:0.95,2:0.975,3:0.975}


df.longer = df.longer.replace({True: 1, False: 0})
df.shorter = df.shorter.replace({True: 1, False: 0})
df.equal = df.equal.replace({True: 1, False: 0})
df['no_n-1'] = df['no_n-1'].replace({True: 1, False: 0})
df['no_n-2'] = df['no_n-2'].replace({True: 1, False: 0})
df['no_n+1'] = df['no_n+1'].replace({True: 1, False: 0})
df['no_n+2'] = df['no_n+2'].replace({True: 1, False: 0})


df = df.drop_duplicates()
y = df['bend_class']
X = df[[col for col in df.columns if col not in ['bend_class', 'song_id']]]
print("\nTraining on the following features:\n")
print(X.columns)
y_binary = y.apply(lambda x: 0 if x == 0 else 1)

# Load test split to get same results as the paper
test_index = np.loadtxt("../data/test_index.txt")
y_test = y.iloc[test_index]
X_test = X.iloc[test_index]
X_train = X.loc[~X.index.isin(y_test.index)]
y_train = y.loc[~y.index.isin(y_test.index)]

y_train_binary = y_binary.loc[~y_binary.index.isin(y_test.index)]
y_test_binary = y_binary.iloc[test_index]
clf = tree.DecisionTreeClassifier(max_depth=None, criterion='gini', class_weight=None, random_state=22)
clf_binary = tree.DecisionTreeClassifier(max_depth=None, criterion='gini', random_state=22)
X_test_feat = X_test
y_test_feat = y_test
clf_feat = clf
clf = clf.fit(X_train, y_train)

path = "../data/trained_models/trained_{0}.joblib".format(datetime.today().strftime('%Y-%m-%d-%H:%M'))

print("Model is trained and saved here: ", path)
dump(clf, path)

clf_binary = clf_binary.fit(X_train, y_train_binary)

np.set_printoptions(precision=2)

cm = confusion_matrix(y_test, clf.predict(X_test))

make_confusion_matrix(cm, group_names=class_names_latex,
        title="Confusion Matrix of classification task")

cm_binary = confusion_matrix(y_test_binary, clf_binary.predict(X_test))


make_confusion_matrix(cm_binary,
        title="Confusion Matrix where bends are aggregated to one class")

plt.show()


print("\nComputing feature importance over 30 iterations...\n")
r = permutation_importance(clf_feat, X_test_feat, y_test_feat,
        n_repeats=30, random_state=2, scoring='f1_macro')
for i in r.importances_mean.argsort()[::-1]:
    if r.importances_mean[i] - 2 * r.importances_std[i] > 0:
        print(f"{X_test.columns[i]:<8}"
              f" {r.importances_mean[i]:.3f}"
              f" +/- {r.importances_std[i]:.3f}")
