# Modeling Bends in Popular Music Guitar Tablatures

## Alexandre D'Hooge, Louis Bigo, Ken Déguernel

Corresponding author: _alexandre.dhooge[at]univ-lille[dot]fr_

## Univ. Lille, CNRS, Centrale Lille, UMR 9189 CRIStAL, F-59000 Lille, France

Tablature notation is widely used in popular music to transcribe and share
guitar musical content.  As a complement to standard score notation, tablatures
transcribe performance gesture information including finger positions and a
variety of guitar-specific playing techniques such as slides, hammer-on/pull-off
or bends. This paper focuses on bends, which enable to progressively shift the
pitch of a note, therefore circumventing physical limitations of the discrete
fretted fingerboard. In this paper, we propose a set of 25 high-level features,
computed for each note of the tablature, to study how bend occurrences can be
predicted from their past and future short-term context. Experiments are
performed on a corpus of 932 lead guitar tablatures of popular music and show
that a decision tree successfully predicts bend occurrences with an F1 score of
0.71 and a limited amount of false positive predictions, demonstrating promising
applications to assist the arrangement of non-guitar music into guitar
tablatures.


## Repository Structure

```bash
.
├── data
├── media
├── README.md
└── src
    ├── data
    └── util
```

## Configuration

### Setting up Python environment

This project runs with __Python 3.10__. After setting up a virtual environment,
you can install the requirements with:

```bash
pip install -r requirements.txt
```

### Music21

__This paragraph should not be necessary if you set up your environment
with the previous command.__

This project uses custom changes made to Music21 that are not **yet** part of
the official [Music21 Library](http://web.mit.edu/music21/).

For this reason, you must clone our custom version of music21, available
[here](https://github.com/adhooge/music21.git). For example:
```
git clone https://github.com/adhooge/music21.git 
```

You can then add this custom library to your project using `pip`:
```
pip install -e /path/to/custom/music21
```

**Warning:** If you do not use a dedicated Python environment, this install
**will** interfere with any existing `music21` install you have.


## Usage

### Parsing a `.gp` file

The modifications made to `music21` allow to parse correctly _bend_ techniques.
Unfortunately, it is not yet possible to parse simple `musicXML` files featuring
those techniques. We provide however the code to parse _GuitarPro_ `.gp` files.

Unzip a `.gp` file to obtain a `.gpif` file. You can then parse it and obtain
the corresponding `music21` _Stream_ by running:

```Python
src/ >>> from data.parserGP import ParserGP
src/ >>> stream = ParserGP().parseFile('/path/to/file.gpif')      # this may take a few seconds
```

If you use this parser, please cite [the paper](https://hal.science/hal-02934382/document) (in French)
that introduced it:

> Jules Cournut, Louis Bigo, Mathieu Giraud, Nicolas Martin. Encodages de tablatures pour l’analyse
> de musique pour guitare. Journées d’Informatique Musicale (JIM 2020), 2020, Strasbourg (en ligne),
> France.

#### Removing bends

TODO

### Extract the features for prediction

Once you have a `music21` stream ready to use, you can extract the features required for
prediction like so:

```Python
src/ >>> from util.feature_extractor import process_stream
src/ >>> df = process_stream(stream)
```

This will provide you with a dataframe containing the features extracted for each note
of the input stream. You can also directly use the data we computed by loading the corresponding
`.csv` file:

```Python
src/ >>> import pandas as pd
src/ >>> df = pd.read_csv("../data/feat_df.csv", index_col=0)
```

### Training a classifier

If you use the provided data, you can retrain a decision tree on the task of bend prediction:

```bash
src/$ python model/model.py
```

### Prediction

You can obtain bend suggestions on an input file with the script in `src/model/predict.py`.
In the `src/` folder, run `python -m model.predict -h` for usage information. Or run directly the example:

```shell
src/$ python -m model.predict
```

The default script uses the pretrained decision tree of the paper for prediction and analyzes
an excerpt from Chopin's Nocturne 2 Op. 9 (file in the `data/` folder). It will print a list of the bend class suggestions for each note
according to the following mapping:
- $0 = ~\varnothing$;
- $1 = ~ \uparrow$;
- $2 = ~ \rightarrow$;
- $3 = ~ \downarrow$.

Please refer to the paper for a detailed explanation of the taxonomy used here. 

Also, keep in mind that tied notes count only as one regarding the prediction.


If you use a `.gpif` file as input, the script should produce an image of the tab with the predictions in lyrics.
The tab will likely have some weird errors like missing lines that are due to current limitations of `music21` and the MusicXML format.


## Citation

If you use this repo in any research work, please cite the corresponding paper:
> Alexandre D'Hooge, Louis Bigo, Ken Déguernel. Modeling Bends in Popular Music Guitar Tablatures. Proceedings of the 24th International Society for Music Information Retrieval Conference, Nov 2023, Milan, Italy. 

Bibtex:

```Bibtex
@inproceedings{dhooge:hal-04142267,
  TITLE = {{Modeling Bends in Popular Music Guitar Tablatures}},
  AUTHOR = {D'Hooge, Alexandre and Bigo, Louis and D{\'e}guernel, Ken},
  URL = {https://hal.science/hal-04142267},
  BOOKTITLE = {{Proceedings of the 24th International Society for Music Information Retrieval Conference}},
  ADDRESS = {Milan, Italy},
  ORGANIZATION = {{International Society for Music Information Retrieval}},
  YEAR = {2023},
  MONTH = Nov,
  KEYWORDS = {Guitar Tablatures ; MIR Music Information Retrieval ; Playing technique recognition},
  PDF = {https://hal.science/hal-04142267/file/Bend_Prediction-4.pdf},
  HAL_ID = {hal-04142267},
  HAL_VERSION = {v1},
}
```

## License

This project is licensed under GNU-GPL v3, refer to the License file for more
details.

## Acknowledgements

This work is made with the support of the French National Research Agency, in
the framework of the project TABASCO
([ANR-22-CE38-0001](https://anr.fr/Projet-ANR-22-CE38-0001)).
